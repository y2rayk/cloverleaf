# cloverleaf

``` python
# Create a virtual environment to isolate our package dependencies locally
python3 -m venv env
source env/bin/activate  # On Windows use `env\Scripts\activate`
pip install -r requirements.txt
python manage.py test # to run unit tests
python manage.py runserver  # to run server

```
