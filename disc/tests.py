from django.test import TestCase
from django.test import Client
import json


class EvaulateTestCase(TestCase):
    def setUp(self):
        pass

    def test_invalid_post_should_error(self):
        c = Client()
        url = '/disc/evaluate'
        data = {
            "ZZZanswers": [
                { "description": 'orderly', "rank": 4 },
            ]
        }
        response = c.post(url, data=json.dumps(data), content_type='application/json')
        self.assertEqual(response.status_code, 400)
        expected = {
            'success': False,
            'scores': {}
        }
        self.assertEqual(response.json(), expected)

    def test_invalid_post_should_error2(self):
        c = Client()
        url = '/disc/evaluate'
        data = {
            "answers": [
                { "XXXdescription": 'orderly', "rank": 4 },
            ]
        }
        response = c.post(url, data=json.dumps(data), content_type='application/json')
        self.assertEqual(response.status_code, 400)
        expected = {
            'success': False,
            'scores': {}
        }
        self.assertEqual(response.json(), expected)

    def test_post_should_evaluate(self):
        c = Client()
        url = '/disc/evaluate'
        data = {
            "answers": [
                #{ "description": "persuasive", "rank": 1 },
                #{ "description": "humble", "rank": 4 }
                { "description": 'persuasive', "rank": 1 },
                { "description": 'gentle', "rank": 0 },
                { "description": 'humble', "rank": 4 },
                { "description": 'original', "rank": 0 },
                { "description": 'aggressive', "rank": 0 },
                { "description": 'life-of-the-party', "rank": 2 },
                { "description": 'easy mark', "rank": 1 },
                { "description": 'fearful', "rank": 4 },

                { "description": 'sweet', "rank": 4 },
                { "description": 'cooperative', "rank": 1 },
                { "description": 'tenacious', "rank": 0 },
                { "description": 'attractive', "rank": 0 },

                { "description": 'cautious', "rank": 4 },
                { "description": 'determined', "rank": 0 },
                { "description": 'convincing', "rank": 0 },
                { "description": 'good-natured', "rank": 1 },

                { "description": 'docile', "rank": 0 },
                { "description": 'bold', "rank": 0 },
                { "description": 'loyal', "rank": 4 },
                { "description": 'charming', "rank": 1 },

                { "description": 'willing', "rank": 0 },
                { "description": 'poised', "rank": 0 },
                { "description": 'agreeable', "rank": 1 },
                { "description": 'high-spirited', "rank": 4 },

                { "description": 'will power', "rank": 0 },
                { "description": 'open-minded', "rank": 1 },
                { "description": 'obliging', "rank": 0 },
                { "description": 'cheerful', "rank": 4 },

                { "description": 'confident', "rank": 0 },
                { "description": 'sympathetic', "rank": 1 },
                { "description": 'tolerant', "rank": 4 },
                { "description": 'assertive', "rank": 0 },

                { "description": 'even-tempered', "rank": 4 },
                { "description": 'precise', "rank": 1 },
                { "description": 'nervy', "rank": 0 },
                { "description": 'jovial', "rank": 0 },

                { "description": 'well-disciplined', "rank": 0 },
                { "description": 'generous', "rank": 1 },
                { "description": 'animated', "rank": 0 },
                { "description": 'persistent', "rank": 4 },

                { "description": 'competitive', "rank": 4 },
                { "description": 'outgoing', "rank": 1 },
                { "description": 'considerate', "rank": 0 },
                { "description": 'harmonious', "rank": 0 },

                { "description": 'admirable', "rank": 1 },
                { "description": 'kind', "rank": 0 },
                { "description": 'resigned', "rank": 4 },
                { "description": 'force of character', "rank": 0 },

                { "description": 'obedient', "rank": 0 },
                { "description": 'fussy', "rank": 4 },
                { "description": 'unconquerable', "rank": 0 },
                { "description": 'playful', "rank": 1 },

                { "description": 'respectful', "rank": 0 },
                { "description": 'pioneering', "rank": 0 },
                { "description": 'optimistic', "rank": 1 },
                { "description": 'accommodating', "rank": 4 },

                { "description": 'brave', "rank": 0 },
                { "description": 'inspiring', "rank": 1 },
                { "description": 'submissive', "rank": 0 },
                { "description": 'timid', "rank": 4 },

                { "description": 'adaptable', "rank": 1 },
                { "description": 'argumentative', "rank": 4 },
                { "description": 'nonchalant', "rank": 0 },
                { "description": 'light-hearted', "rank": 0 },

                { "description": 'gregarious', "rank": 0 },
                { "description": 'patient', "rank": 4 },
                { "description": 'self-reliant', "rank": 1 },
                { "description": 'soft-spoken', "rank": 0 },

                { "description": 'contented', "rank": 4 },
                { "description": 'trusting', "rank": 0 },
                { "description": 'peaceful', "rank": 0 },
                { "description": 'positive', "rank": 1 },

                { "description": 'adventurous', "rank": 1 },
                { "description": 'receptive', "rank": 0 },
                { "description": 'cordial', "rank": 0 },
                { "description": 'moderate', "rank": 4 },

                { "description": 'lenient', "rank": 0 },
                { "description": 'aesthetic', "rank": 4 },
                { "description": 'vigorous', "rank": 1 },
                { "description": 'good-mixer', "rank": 0 },

                { "description": 'talkative', "rank": 4 },
                { "description": 'controlled', "rank": 0 },
                { "description": 'conventional', "rank": 0 },
                { "description": 'decisive', "rank": 1 },

                { "description": 'restrained', "rank": 4 },
                { "description": 'accurate', "rank": 0 },
                { "description": 'outspoken', "rank": 1 },
                { "description": 'companionable', "rank": 0 },

                { "description": 'diplomatic', "rank": 1 },
                { "description": 'audacious', "rank": 0 },
                { "description": 'polished', "rank": 0 },
                { "description": 'satisfied', "rank": 4 },

                { "description": 'restless', "rank": 1 },
                { "description": 'popular', "rank": 0 },
                { "description": 'neighborly', "rank": 0 },
                { "description": 'orderly', "rank": 4 },

            ]
        }
        response = c.post(url, data=json.dumps(data), content_type='application/json')
        self.assertEqual(response.status_code, 200)
        expected = {
            'success': True,
            'scores': {
                'd': 3,
                'i': 5,
                's': -2,
                'c': -2
            }
        }
        self.assertEqual(response.json(), expected)

    def test_post_should_evaluate2(self):
        c = Client()
        url = '/disc/evaluate'
        data = {
            "answers": [
                { "description": 'persuasive', "rank": 1 },
                { "description": 'gentle', "rank": 2 },
                { "description": 'humble', "rank": 4 },
            ]
        }
        response = c.post(url, data=json.dumps(data), content_type='application/json')
        self.assertEqual(response.status_code, 200)
        expected = {
            'success': True,
            'scores': {
                'd': 0,
                'i': 1,
                's': 0,
                'c': 0
            }
        }
        self.assertEqual(response.json(), expected)
